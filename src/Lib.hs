module Lib
  ( someFunc
  ) where

import           Lib.Prelude

someFunc :: IO ()
someFunc = putText "someFunc"

