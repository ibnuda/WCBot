{-# LANGUAGE DeriveFunctor              #-}
{-# LANGUAGE DeriveGeneric              #-}
{-# LANGUAGE GeneralizedNewtypeDeriving #-}
module Type where

import           Lib.Prelude

import           Data.Aeson
import           Data.Aeson.Casing (aesonPrefix, snakeCase)
import           Servant.Server    (ServantErr)
import           Servant.Client
import           Servant.API
import           Telegram.Bot.API

data Statistic = Statistic
  { statisticAttemptsOnGoal  :: Int
  , staticticBallPossession  :: Int
  , staticticBallsRecovered  :: Int
  , staticticBlocked         :: Int
  , staticticClearances      :: Int
  , staticticCorners         :: Int
  , staticticCountry         :: Text
  , staticticDistanceCovered :: Int
  , staticticFoulsCommitted  :: Int
  , staticticNumPasses       :: Int
  , staticticOffTarget       :: Int
  , staticticOnTarget        :: Int
  , staticticPassAccuracy    :: Int
  , staticticPassesCompleted :: Int
  , staticticRedCards        :: Int
  , staticticTackles         :: Int
  , staticticWoodwork        :: Int
  , staticticYellowCards     :: Int
  } deriving (Generic, Show)

instance ToJSON Statistic where
  toJSON = genericToJSON $ aesonPrefix snakeCase

instance FromJSON Statistic where
  parseJSON = genericParseJSON $ aesonPrefix snakeCase

data Event = Event
  { eventId      :: Int
  , eTypeOfEvent :: Text
  , ePlayer      :: Text
  , eTime        :: Text
  } deriving (Generic, Show)

instance ToJSON Event where
  toJSON = genericToJSON $ aesonPrefix snakeCase

instance FromJSON Event where
  parseJSON = genericParseJSON $ aesonPrefix snakeCase

data Team = Team
  { teamcountry :: Text
  , teamCode    :: Text
  , teamGoals   :: Int
  } deriving (Generic, Show)

instance ToJSON Team where
  toJSON = genericToJSON $ aesonPrefix snakeCase

instance FromJSON Team where
  parseJSON = genericParseJSON $ aesonPrefix snakeCase

data Match = Match
  { matchAwayTeam           :: Team
  , matchAwayTeamEvents     :: [Event]
  , matchAwayTeamStatistics :: Maybe Statistic
  , matchDatetime           :: Text
  , matchFifaId             :: Text
  , matchHomeTeam           :: Team
  , matchHomeTeamEvents     :: [Event]
  , matchHomeTeamStatistics :: Maybe Statistic
  , matchLastEventUpdateAt  :: Maybe Text
  , matchLastScoreUpdateAt  :: Maybe Text
  , matchLocation           :: Text
  , matchTime               :: Maybe Text
  , matchVenue              :: Maybe Text
  , matchWinner             :: Maybe Text
  , matchWinnerCode         :: Maybe Text
  } deriving (Generic, Show)

instance ToJSON Match where
  toJSON = genericToJSON $ aesonPrefix snakeCase

instance FromJSON Match where
  parseJSON = genericParseJSON $ aesonPrefix snakeCase

data Configuration = Configuration
  { configurationLastEventId   :: MVar Int
  , configurationMatchTodayUrl :: Text
  , configurationChatId        :: ChatId
  , configurationClientEnv     :: ClientEnv
  }

newtype AppT m a = AppT
  { runApp :: ReaderT Configuration (ExceptT ServantErr m) a
  } deriving ( Functor
             , Applicative
             , Monad
             , MonadReader Configuration
             , MonadError ServantErr
             , MonadIO
             )

type App = AppT IO

-- Telegram
type ResponseMessage = Telegram.Bot.API.Response Message
newtype Token =
  Token Text
  deriving (Show, Eq, ToHttpApiData, FromHttpApiData)
