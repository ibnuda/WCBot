{-# LANGUAGE FlexibleContexts #-}
module Internal where

import           Lib.Prelude

import qualified Data.ByteString.Lazy       as BL
import           Data.Text
import           Network.HTTP.Conduit
import           Servant.Client
import           Telegram.Bot.API

import           Type

telegramBaseUrl :: BaseUrl
telegramBaseUrl = BaseUrl Https "api.telegram.org" 443 ""

lookupToday'sMatch :: MonadIO m => Text -> m Text
lookupToday'sMatch url =
  (decodeUtf8 . BL.toStrict) <$> (simpleHttp . unpack $ url)

sendMatchRes ::
     (MonadReader Configuration m, MonadIO m)
  => m (Either ServantError ResponseMessage)
sendMatchRes = do
  url <- asks configurationMatchTodayUrl
  chatid <- asks configurationChatId
  anu <- lookupToday'sMatch url
  clientenv <- asks configurationClientEnv
  liftIO $ flip runClientM clientenv $
    sendMessage
      SendMessageRequest
        { sendMessageChatId = SomeChatId chatid
        , sendMessageText = anu
        , sendMessageParseMode = Just Markdown
        , sendMessageDisableNotification = Nothing
        , sendMessageDisableWebPagePreview = Nothing
        , sendMessageReplyToMessageId = Nothing
        , sendMessageReplyMarkup = Nothing
        }
